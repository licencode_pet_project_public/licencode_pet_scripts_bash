#! /bin/bash
EXIT_FIRST_MENU=1
EXIT_SECOND_MENU=1

echo ""

echo "Your card a:"

# Show grahpic card select now
glxinfo | grep 'OpenGL renderer string'
echo ""

echo "Input 1 for change video card"
echo "Input 0 for exit"

while [ $EXIT_FIRST_MENU -eq 1 ]; do
    read FIRST_MENU_CHANGE_OR_EXIT
    case $FIRST_MENU_CHANGE_OR_EXIT in
        1) 
            echo "Input 1 for select Intel card"
            echo "Input 2 for select Nvidia card"
            echo "Input 0 for exit"
    
            while [ $EXIT_SECOND_MENU -eq 1 ]; do
                read SELECT_VIDEO_CARD
                case $SELECT_VIDEO_CARD in
                    1) 
                        echo "INTEL"
                    ;;
                    2) 
                        echo "NVIDIA"
                    ;;
                    0) 
                        EXIT_SECOND_MENU=0
                        EXIT_FIRST_MENU=0
                    ;;
                    *) 
                        echo "I don't know what is select."
                        echo "Input 1 for select Intel card"
                        echo "Input 2 for select Nvidia card"
                        echo "Input 0 for exit"
                    ;;
                esac
            done
        ;;
        0) 
            EXIT_FIRST_MENU=0
        ;;
        *) 
            echo "I don't know what is select."
            echo "Input 1 for change video card"
            echo "Input 0 for exit"
        ;;
     esac
done